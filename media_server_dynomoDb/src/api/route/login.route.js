const express=require('express');
const Login_controller=require( "../auth/login/login.controller");

const router = express.Router();

router.post("/login",
    Login_controller.tokenGen );


module.exports = router;