const express=require('express');
const Register_controller=require('../auth/register/register.controller')



const router = express.Router();

router.post("/user",Register_controller.userinsert);

router.get("/users",Register_controller.find);

router.get("/user/:id", Register_controller.findOne );


module.exports = router;