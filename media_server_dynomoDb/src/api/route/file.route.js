const express=require('express');

const file_controller=require('../File_handle/file.controller');
const multiparty=require('connect-multiparty'),multipartyMiddleware = multiparty();

const router = express.Router();
// router.use(multipartyMiddleware)
router.post("/file",multipartyMiddleware, file_controller.filePut)

router.get("/files",file_controller.AllPublicFile)

router.get("/userfiles",file_controller.FindUserfile)


router.post("/updateAccess",file_controller.modifyAccess)
module.exports = router;