'use strict';

// require('doctenv').config();

module.exports={

    app:{
        name:'mediaserver',
        baseUrl:'http://localhost:',
        port:4000,
    },

    api:{
        prefix: '^/api/v[1-9]',
        version: [1],
    },

    database:{
        url:'mongodb://localhost:27017/serverMedia'
    }


}