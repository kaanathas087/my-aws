const express=require('express');
const bodyparser=require('body-parser');
const vm=require('v-response');

const config=require('config');
// const fileUp=require('express-fileupload')

const register_route=require('./src/api/route/register.route');
const login_route=require('./src/api/route/login.route');
const file_route=require('./src/api/route/file.route')
// const db=require('./config/DbCon')
const prefix=config.get('api.prefix');
const multiparty=require('connect-multiparty'),multipartyMiddleware = multiparty();
const app=express();
// app.use(multipartyMiddleware())
// app.use(fileUp());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization,x-api-key");
    next();
});

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: true}));
app.use(prefix, register_route);
app.use(prefix, login_route);
app.use(prefix, file_route)
// db();
module.exports=app





