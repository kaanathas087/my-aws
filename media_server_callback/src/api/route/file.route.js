const express=require('express');

const file_controller=require('../File_handle/file.controller');


const router = express.Router();

router.post("/file",file_controller.create);

router.get("/files",file_controller.AllPublicFile)

router.get("/userfiles",file_controller.FindUserfile)


router.post("/updateAccess",file_controller.modifyAccess)
module.exports = router;