const mongoose=require('mongoose');

const Schema=mongoose.Schema;

const fileSchema=new Schema({
    user_id:{
        type:String
    },
    file_name:{
        type:String,
        // required:true
    },

    file_path:{
        type:String,
        required:true
    },

    file_private:{
        type:Boolean
    },
    file_type:{
        type:String,
        // required:true
    }



});



const files = mongoose.model('file', fileSchema,"files");
module.exports = files;