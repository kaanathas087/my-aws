const mongoose=require('mongoose');

const Schema=mongoose.Schema;

const userSchema=new Schema(
    {
        first_name: {
            type: String,
            required: true
        },
        last_name: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true
        },
        password: {
            type: String,
            required: true
        },
        token:{
            type:String
        
        }

    }
);


 const register = mongoose.model('user', userSchema,"register");
module.exports = register;