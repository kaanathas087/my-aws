const express=require('express');
const bodyparser=require('body-parser');
const vm=require('v-response');
// const mongoose=require('mongoose');
const config=require('config');
const fileUp=require('express-fileupload')

const register_route=require('./src/api/route/register.route');
const login_route=require('./src/api/route/login.route');
const file_route=require('./src/api/route/file.route')
const db=require('./config/DbCon')

const port=config.get('app.port');
const prefix=config.get('api.prefix');

const app=express();

app.use(fileUp());
app.use(function(req,res,next){
    res.header("Access-Control-Allow-Origin", "*");
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept,Authorization,x-api-key");
    next();
});

app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended: false}));
app.use(prefix, register_route);
app.use(prefix, login_route);
app.use(prefix,file_route)

// mongoose.connect(db,{ useNewUrlParser: true,useUnifiedTopology: true  } )
//         .then(() => vm.log("connected to mongoDB", db))
//         .catch(err => vm.log("error mongodb", err));

db();

module.exports=app







// var assert = require('assert');
// const file=require('../src/api/File_handle/file.controller')
// const expect=require('chai').expect;
// const request=require('supertest');
// const app=require('../app')
// const index=require('../index')
// const connect=require('../config/DbCon')



// describe('check  controles',()=>{
//     // before((done)=>{
//     //     index()
//     //     done()

//     // })


//     it('POST /user',(done)=>{
        
//         request(app).post('/user').send({
//             first_name:"kaaanathas",
//             last_name:"thas",
//             email:"kaanadfd098@gnail",
//             password:"12345678"

//         }) .expect(201).end((err,res)=>{
//              if (err) return done(err);
//            done()
//          })
//     })

//     // it('get/files',(done)=>{
//     //     request(app).get('/files').expect(200, done);
//     // })