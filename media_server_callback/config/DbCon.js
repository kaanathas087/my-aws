const mongoose=require('mongoose');
const config=require('config');

const vm=require('v-response');
const dbhost=config.get('database.url');


var connect=(req,res)=>{
    mongoose.connect(dbhost, )
    .then(() => vm.log("connected to mongoDB", dbhost))
    .catch(err => vm.log("error mongodb", err));

};

module.exports=connect;