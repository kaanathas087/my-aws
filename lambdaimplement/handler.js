'use strict';
const aws=require('aws-sdk')
const bcrypt=require('bcryptjs')
const dynamodb=new aws.DynamoDB.DocumentClient();


module.exports.hello = async event => {
  return {
    statusCode: 200,
    body: JSON.stringify(
      {
        message: 'Go Serverless v1.0! Your function executed successfully!',
        input: event,
      },
      null,
      2
    ),
  };

  // Use this code if you don't use the http event with the LAMBDA-PROXY integration
  // return { message: 'Go Serverless v1.0! Your function executed successfully!', event };
};

function finduserByEmail(email){
  console.log("this is by email",email)
  return new Promise((resolve,reject)=>{
     dynamodb.query({
       TableName:'usert',
       KeyConditionExpression:'#email=:email',
       ExpressionAttributeNames:{
         '#email':'email'
       },ExpressionAttributeValues:{
         ':email':email
       }
     },(err,data)=>{
       console.log(data,"this respond in email find")
       if(data.Items[0]){
         resolve(false)
       }else{
         resolve(true)
       }
       if(err){
         reject(err)
       }

     })

  })
}

function insertuser(body){
   return new Promise((resolve,reject)=>{
     console.log("insert in to dynamodb",body)
     bcrypt.genSalt(10,(err,salt)=>{
       bcrypt.hash(body.password,salt,(err,hash)=>{
         body.password=hash;
         dynamodb.put({
           TableName:'usert',
           Item:{
             email:body.email,
             User_id:Date.now(),
             First_name:body.first_name,
             password:body.password,
           }
         },(err,data)=>{
            if(err){
              reject(err)
            }else{
              resolve(data)
            }

         })
       })
     })
   })


}

function createUser(body){
 console.log(body)
  return new Promise((resolve,reject)=>{
    finduserByEmail(body.email)
    .then(res=>{
     if(!res){
       return reject(404)
 
     }else{
       insertuser(body)
     }
    })
    .then(resolve(201))
    .catch(err=>{reject(err)})
 
    });

  }
 


module.exports.Ucreate=(event,context,callback)=>{
  const requestbody=JSON.parse(event.body)

   createUser(requestbody).then(res=>{if(res){
    callback(null, {statusCode: 200,body:JSON.stringify({message:requestbody})});

   }
   
   }).catch(err=>{
    callback(null, {statusCode:201,body: JSON.stringify({ message: `Unable to submit `})})
   })

}



 function emailcheck(email,pass){
  console.log("before find user",email,pass)
  return new Promise((resolve,reject)=>{
     dynamodb.query({
        TableName:'usert',
        KeyConditionExpression:"#email=:email",
        ExpressionAttributeNames:{
          '#email':'email'
        },
        ExpressionAttributeValues:{
          ':email':email
        }
      },(err,data)=>{
        console.log("after find user",data)
        if(data.Items[0]){
          console.log("after2 find user",data)
          verifypass(pass,data.Items[0].password)
          .then(ver=>{
            if(ver){
              resolve([true,data.Items[0]])
            }else{
              reject("incorrect password",ver)
            }
          })
        }else{
          resolve(false)
        }
        if(err){
          reject(err)
        }

      })
  })

}


function verifypass(Inpass,RetrivePass){
  return new Promise((resolve,reject)=>{
    bcrypt.compare(Inpass,RetrivePass)
    .then(result=>{
      if(result){
        resolve(result)
      }else{
        resolve(result)
      }
    })
    .catch(err=>{
      reject(err)
    })
  })
}


function goInserToken(data){
  return new Promise((resolve,reject)=>{
    if(data[0]){
      userData=data[1]
      const payload={id:userData.User_id}
      jwt.sign(payload,"keys",{expireIn:"365"},(error,token)=>{
        if(!error){
          resolve([token,userData])
        }else{
          reject(error)
        }
      })
    }
  })

}
 function insertToken(datas){
  let token=datas[0]
  let data=datas[1]
  console.log('toen is',token)
  return new Promise((resolve,reject)=>{
    dynamodb.put({
      TableName:"usert",
      Item:{
        User_id:data.User_id,
        // First_name:data.First_name,
        password:data.password,
        email:data.email,
        User_token:token
      },ConditionExpression:'#email=:useremail',
      ExpressionAttributeNames:{
          '#email':'email'
      },ExpressionAttributeValues:{
          ':useremail':data.email
      }
    },(err,data)=>{
      if(err){
          reject(err)
      }else{
          console.log("after inser the data into dynamo",token)
          resolve(token)
       }})
  })
}
 function tokengenatration(email,pass){
  console.log("token genatration enter")

 return new Promise((resolve,reject)=>{
     let token;
     emailcheck(email,pass)
     .then(goInserToken)
     .then(tok=>{
         console.log("befor insert token",tok)
         insertToken(tok)})
     .then(data=>{
         // if(data){
         console.log("update token res",data)
         resolve(data)
     // }else{
         // reject(302)
     // }
 }
     )
     .catch(err=>{
         console.log(" in insert token",err)
         reject(err)
     })
 })

}



module.exports.login=async (event,context,callback)=>{
  const req=JSON.parse(event.body)
  await tokengenatration(req.email,req.password)
  .then(res=>{
    callback(null,{statusCode:200,body:JSON.stringify({ message:'login success'})})
  }).catch(err=>{
    callback(null,{statusCode:404,body:JSON.stringify({message:'login fail'})}
    )
  })
}



// module.exports.Ulogin=(event,context,callback)=>{

// }

// module.exports.Fupload=(event,context,cllback)=>{

// }